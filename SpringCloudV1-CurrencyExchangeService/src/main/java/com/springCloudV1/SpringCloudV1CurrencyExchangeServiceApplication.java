package com.springCloudV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringCloudV1CurrencyExchangeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV1CurrencyExchangeServiceApplication.class, args);
	}

}
