package com.springCloudV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix 
public class SpringCloudV1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV1Application.class, args);
	}

}
