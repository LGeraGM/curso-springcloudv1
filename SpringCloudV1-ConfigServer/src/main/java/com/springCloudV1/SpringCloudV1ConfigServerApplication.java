package com.springCloudV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class SpringCloudV1ConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV1ConfigServerApplication.class, args);
	}

}
