package com.springCloudV1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.*;
import org.springframework.context.annotation.Bean;
import brave.sampler.Sampler;

@SpringBootApplication
@EnableFeignClients("com.springCloudV1")
@EnableDiscoveryClient
public class SpringCloudV1CurrencyConversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudV1CurrencyConversionServiceApplication.class, args);
	}

	@Bean
	public Sampler defaultSampler(){
		return Sampler.ALWAYS_SAMPLE;
	}
}
